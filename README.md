#CWP training project

A series of step-by-step tutorials which introduce basic concepts of SilverStripe as used in the NZ Government Common Web Platform.

These are aimed at a SilverStripe beginner audience and assumes some prior PHP, HTML and CSS knowledge. Some knowledge of git version control is also useful.
