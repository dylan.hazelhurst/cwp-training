<?php
class Page extends BasePage {

	private static $db = array(
		'RightContent' => 'HTMLText',
		'Abstract' => 'Text'
	);

	private static $has_one = array(
	);

	public function getCMSFields(){

		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Right Content',
			HTMLEditorField::create('RightContent', 'Right-hand content')
		);

		$fields->addFieldToTab('Root.Main',
			TextareaField::create('Abstract'),
			'URLSegment'
		);

		return $fields;
	}

}

class Page_Controller extends BasePage_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function getBaseStyles() {
		$styles = parent::getBaseStyles();

		$themeDir = SSViewer::get_theme_folder();
		array_push($styles['screen'], "$themeDir/css/styles.css");
		return $styles;
	}

}
