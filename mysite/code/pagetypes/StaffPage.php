<?php
class StaffPage extends Page {

	private static $description = 'A staff member page';

	private static $can_be_root = false;

	private static $db = array(
		'JobTitle' => 'Varchar'
	);

	private static $has_one = array(
		'Photo' => 'Image'
	);

	private static $has_many = array(
		'Endorsements' => 'StaffEndorsement'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Title')->Title = "Staff name";

		$fields->addFieldToTab(
			'Root.Main',
			TextField::create('JobTitle', 'Job title'),
			'URLSegment'
		);

		$fields->addFieldToTab(
			'Root.Main',
			UploadField::create('Photo', 'Profile photo'),
			'Content'
		);

		$fields->addFieldToTab( 'Root.Endorsements',
			GridField::create(
				'Endorsements',
				'Endorsements',
				$this->Endorsements(),
				GridFieldConfig_RecordEditor::create()
			)
		);
	
		return $fields;
	}
}

class StaffPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
		'EndorsementForm'
	);

	public function EndorsementForm() {

		$fields = FieldList::create(
			TextField::create('EndorsedBy', 'Your name'),
			TextareaField::create('Comments', 'Comments')
		);

		$actions = FieldList::create(
			FormAction::create('addEndorsement', 'Endorse')
		);

		$validator = RequiredFields::create(array('EndorsedBy','Comments'));

		$form = Form::create($this, 'EndorsementForm', $fields, $actions, $validator);

		return $form;
	}

	public function addEndorsement($data, Form $form) {

		$endorsement = new StaffEndorsement();

		$form->saveInto($endorsement);

		$endorsement->StaffID = $this->ID;

		$endorsement->write();

		$form->sessionMessage( 'Thanks for endorsing '. $this->Title, 'good' );

		$this->redirectBack();
	}

	public function PagedEndorsements() {

		$list = new PaginatedList(
			$this->Endorsements()->Sort('Created DESC'),
			$this->request->getVars()
		);

		$list->setPageLength(2);

		return $list;
	}


}
