<?php
class StaffEndorsement extends DataObject {

	private static $db = array(
		'EndorsedBy' => 'Varchar(255)',
		'Comments' => 'Text'
	);

	private static $has_one = array(
		'Staff' => 'StaffPage'
	);

	private static $summary_fields = array(
		'Created.Nice' => 'Date endorsed',
		'EndorsedBy' => 'Endorsed by',
		'Comments' => 'Comments'
	);

	private static $default_sort = 'Created DESC';

}
