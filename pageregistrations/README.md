# Page Registrations

Provides a basic registration form mechanism which can be attached to any Page type.

## Requirements

 * silverstripe/framework ~3.1
 * silverstripe/cms ~3.1

## Installation

```
composer require camfindlay/pageregistrations
```

## Configuration

### Applying the extension

Add the following to your project config.yml adjust depending on which page type you want to attach the form to.

```yml
MyPage:
  extensions:
    - RegistrationExtension
MyPage_Controller:
  extensions:
    - RegistrationControllerExtension
```

Make sure to run a 'dev/build' after changing the yml to build the database changes.

### Including the form
There are various options to include the form:

 * Include the template tag $RegistrationForm in your page layout
 * Use the supplied include template <% include EventRegistration %>
 * Override the template include in your theme folder and then include as above

## License
Copyright (c) 2014, Cam Findlay <cam@camfindlay.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
