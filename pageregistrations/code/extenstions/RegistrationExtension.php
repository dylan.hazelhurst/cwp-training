<?php
class RegistrationExtension extends DataExtension {
	private static $db = array(
		'EnableRegistrations' => 'Boolean',
		'MaxRegistrations' => 'Int'
	);

	private static $has_many = array(
		'Registrations'=>'Registration',
	);

	public function updateSettingsFields( FieldList $fields ) {
		$fields->addFieldToTab(
			"Root.Registrations",
			FieldGroup::create(
				CheckboxField::create('EnableRegistrations','Allow registrations for this event')
			)->setTitle('Enable')
		);

		$fields->addFieldToTab(
			'Root.Registrations',
			TextField::create('MaxRegistrations', 'Maximum registrations?')
		);
	}

	public function updateCMSFields( FieldList $fields ) {
		if ( $this->owner->EnableRegistrations ) {
				$fields->addFieldToTab(
					'Root.Registrations',
					GridField::create(
						'Registrations',
						'Registrations',
						$this->owner->Registrations(),
						GridFieldConfig_RecordEditor::create()
					)
				);
		}
	}

}


class RegistrationControllerExtension extends DataExtension {

	private static $allowed_actions = array('RegistrationForm');

	public function RegistrationForm(){

		$fields = FieldList::create(
			TextField::create('Name'),
			TextField::create('Organisation'),
			EmailField::create('Email')
		);

		$actions = FieldList::create(
			FormAction::create('doRegister','Send Registration')
		);

		$validator = RequiredFields::create( array('Name','Email') );

		$form =  Form::create($this->owner, 'RegistrationForm', $fields, $actions, $validator);

		return $form;
	}

	public function doRegister($data, Form $form){

		if( $this->SpacesAvailable() ){

			$registration = Registration::create();

			$form->saveInto($registration);

			$registration->PageID = $this->owner->ID;

			$registration->write();

			$form->sessionMessage('Thanks for registering','good');

		} else {

			$form->sessionMessage('Registrations are full','warning');

		}

		return $this->owner->redirectBack();

	}

	public function SpacesAvailable(){

		$registrations = Registration::get()
			->filter('PageID', $this->owner->ID)
			->exclude('Status', 'Applied')
			->count();

		$spacesleft = (int)($this->owner->MaxRegistrations - $registrations);

		return ($spacesleft > 0) ? $spacesleft : 0;

	}

}
